import email
import imaplib
from collections import Counter
import re

USER_NAME = "your_account_name"
USER_PASSWORD = "your_account_password"

ORG_EMAIL = "@gmail.com"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT = 993

FULL_EMAIL_NAME = USER_NAME + ORG_EMAIL


def read_email_from_gmail():
    # try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER)
        mail.login(FULL_EMAIL_NAME, USER_PASSWORD)

        # Specify the folder for scanning
        mail.select('inbox')

        type, data = mail.search(None, 'ALL')
        mail_ids = data[0]

        id_list = mail_ids.split()
        first_email_id = int(id_list[0])
        latest_email_id = int(id_list[-1])

        from_list = []


        for i in range(latest_email_id, first_email_id, -1):
        # for i in range(20, 1, -1):
            typ, data = mail.fetch(str(i), '(RFC822)')

            for response_part in data:
                if isinstance(response_part, tuple):
                    msg = email.message_from_bytes(response_part[1])
                    email_subject = msg['subject']
                    email_from = msg['from']

                    email_text = re.findall(r"@.+[.].{1,3}",email_from)[0]
                    # print(email_text)
                    from_list.append(email_text+"\n")
                    # print("id:{} from: {}".format(i, email_from))
                    # print('Subject : ' + email_subject + '\n')
                    print(i)

        my_map = Counter(from_list)
        result_list = sorted(my_map.items(), key=lambda x: x[1], reverse=True)
        # print(result_list)

        with open("fromList.txt", "w") as f:
            for item in result_list:
                f.write("{}: {}".format(item[1], item[0]))

    # except Exception as e:
    #     print(str(e))


if __name__ == '__main__':
    read_email_from_gmail()
